<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiCallMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_call_metrics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_category_id');
            $table->unsignedInteger('calls')->default(0);
            $table->date('date');
            $table->tinyInteger('hour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_call_metrics');
    }
}
