<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Admin\Post\Index as PostIndex;
use App\Http\Livewire\Admin\Post\Create as PostCreate;
use App\Http\Livewire\Admin\Post\Edit as PostEdit;

use App\Http\Livewire\Admin\PostCategory\Index as PostCategoryIndex;
use App\Http\Livewire\Admin\PostCategory\Create as PostCategoryCreate;
use App\Http\Livewire\Admin\PostCategory\Edit as PostCategoryEdit;

use App\Http\Livewire\Admin\Settings\Index as SettingsIndex;
use App\Http\Livewire\Admin\Admob\Index as AdmobIndex;

// v2
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('admin.post-category.index'));
});

Route::get('/template/download', function () {
    $file = 'GuideApp.zip';
    $destinationPath = public_path()."/upload/";
    if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
    return response()->download($destinationPath.$file);
});

Route::get('/admin', function () {
    return redirect(route('admin.post-category.index'));
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () 
{
    Route::get('admin/post/{id}', PostIndex::class)->name('admin.post.index');
    Route::get('admin/post/create/{id}', PostCreate::class)->name('admin.post.create');
    Route::get('admin/post/edit/{id}', PostEdit::class)->name('admin.post.edit');

    Route::get('admin/post-category', PostCategoryIndex::class)->name('admin.post-category.index');
    Route::get('admin/post-category/create', PostCategoryCreate::class)->name('admin.post-category.create');
    Route::get('admin/post-category/edit/{id}', PostCategoryEdit::class)->name('admin.post-category.edit');

    Route::get('admin/setting/{id}', SettingsIndex::class)->name('admin.setting.index');
    Route::get('admin/admob/{id}', AdmobIndex::class)->name('admin.admob.index');


    Route::get('v2/home', HomeController::class)->name('home');
});