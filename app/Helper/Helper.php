<?php

function collection($array)
{
    return json_decode(collect($array)->toJson());
}

function duit($nom)
{
    return "Rp. " . (substr($str = chunk_split(strrev($nom), 3, '.'), -1) !== '.' ? strrev($str) : strrev(substr($str, 0, -1)));
}

function nohellchar($string)
{
    return preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', strtolower($string)));
}

function file_name($name, $extension)
{
    return nohellchar($name) . '.' . $extension;
}

?>
