<?php

use App\Models\Post;
use App\Models\PostCategory;

function post()
{
    return new Post;
}

function postCategory()
{
    return new PostCategory;
}
