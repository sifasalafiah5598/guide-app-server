<?php

use App\Library\Dates;
use App\Library\Images;
use App\Library\Rupiah;
use App\Library\Phone;

function _date()
{
    return new Dates;
}

function image()
{
    return new Images;
}

function rupiah()
{
    return new Rupiah;
}

function phone()
{
    return new Phone;
}

?>
