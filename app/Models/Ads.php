<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_category_id', 'banner', 'interstitial', 'open', 'native', 'app_id'
    ];

    public function postCategory()
    {
        return $this->belongsTo('App\Models\PostCategory')->withTrashed();
    }
}
