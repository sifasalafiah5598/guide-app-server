<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostCategory extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'name', 'publisher_id', 'privacy_policy', 'package_name',
    ];
    
    public function ads(){
        return $this->hasOne('App\Models\Ads');
    }

    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function apis(){
        return $this->hasMany('App\Models\ApiCallMetric');
    }
}
