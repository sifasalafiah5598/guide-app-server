<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ApiCallMetric extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'calls', 'date', 'hour'
    ];

    public function postCategory()
    {
        return $this->belongsTo('App\Models\PostCategory')->withTrashed();
    }

    public function scopeNow(Builder $builder)
    {
        return $builder->where([
            'date' => now()->format('Y-m-d'),
            'hour' => now()->format('H')
        ]);
    }

    public function scopeToday(Builder $builder)
    {
        return $builder->where('date', now()->format('Y-m-d'));
    }
}
