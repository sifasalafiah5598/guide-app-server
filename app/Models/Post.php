<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'image_url', 'post_category_id', 'post_content', 'flag_active'
    ];

    public function postCategory()
    {
        return $this->belongsTo('App\Models\PostCategory')->withTrashed();
    }

}
