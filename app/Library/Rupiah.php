<?php
namespace App\Library;

/**
 * Kelas untuk memanipulasi data yang berkaitan dengan rupiah.
 * 
 * Catatan: Saya lupa darimana contoh fungsi terbilang awalnya!
 *
 * @version		0.0.1
 * @author 		Anggiajuang Patria <anggiaj@segellabs.com>
 * @copyright	(c) 2009-2010 http://www.segellabs.com/
 * @license		http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Rupiah
{
	/**
	 * Memformat suatu angka menjadi format yang umum digunakan dalam penulisan nominal rupiah
	 * 
	 * @param 	float 	nilai rupiah
	 * @return 	string
	 */
	public static function format($nominal, $sign = 'Rp. ', $presisi = 0)
	{
		return $sign.number_format($nominal, $presisi, ',', '.');
	}
	
	/**
	 * Mendapatkan nilai mata uang asing
	 *
	 * @param	string	konstanta singkatan mata uang asing, ex: USD, GBP, dst.
	 * @return	float
	 */
	public static function nilai_tukar($mata_uang_asing)
	{
		$url = 'http://quote.yahoo.com/d/quotes.csv?s='
				.strtoupper($mata_uang_asing).'IDR=X&f=a';
				
		if (!$data = file($url)) {
			throw new Exception('Tidak dapat mengambil data dari:'.$url);
		} else {
			return $data[0];
		}
	}
	
	/**
	 * Mengkonversi rupiah ke mata uang asing
	 * 
	 * @param	float	jumlah rupiah
	 * @param	float	nilai tukar mata mata uang asing
	 * @param	float	jumlah pecahan yang ditampilkan
	 * @return	float
	 */
	public static function konversi($nominal_rupiah, $harga_uang_asing, $presisi = 2) 
	{
		return round($nominal_rupiah/$harga_uang_asing, $presisi);
	}
	
	/**
	 * Menyebut nominal rupiah
	 *
	 * Saya bener-bener lupa!!!! & males googling lg :) Jika milik Anda silahkan klaim!!!
	 *
	 * @param	float	nominal rupiah
	 * @return	string
	 */
	public static function terbilang($nominal)
	{
		$nominal = abs($nominal);
		$angka = array(	'', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 
						'tujuh', 'delapan',	'sembilan',	'sepuluh', 'sebelas');
						
		if ($nominal < 12) {
			return ' '.$angka[$nominal];
		}
		
		if ($nominal < 20) {
			return rupiah::terbilang($nominal - 10). ' belas';
		}
		
		if ($nominal < 100) {
			return rupiah::terbilang($nominal / 10). ' puluh'
					. rupiah::terbilang($nominal % 10);
		}
		
		if ($nominal < 200) {
			return ' seratus'. rupiah::terbilang($nominal - 100);
		}
		
		if ($nominal < 1000) {
			return rupiah::terbilang($nominal / 100). ' ratus'
					. rupiah::terbilang($nominal % 100);
		}
		
		if ($nominal < 2000) {
			return ' seribu'. rupiah::terbilang($nominal - 1000);
		}
		
		if ($nominal < 1000000) {
			return rupiah::terbilang($nominal / 1000). ' ribu' 
					. rupiah::terbilang($nominal % 1000);
		}
		
		if ($nominal < 1000000000) {
			return rupiah::terbilang($nominal / 1000000). ' juta' 
					. rupiah::terbilang($nominal % 1000000);
		}
		
		if ($nominal < 1000000000000) {
			return rupiah::terbilang($nominal / 1000000000). ' milyar'
					. rupiah::terbilang(fmod($nominal, 1000000000));
		}
		
		if ($nominal < 1000000000000000) {
			return rupiah::terbilang($nominal / 1000000000000). ' trilyun'
					. rupiah::terbilang(fmod($nominal, 1000000000000));
		}		
	}
}