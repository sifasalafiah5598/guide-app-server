<?php

namespace App\Library;

use Carbon\Carbon;

class Dates
{
    private static $params;

    private static $maps = [
        'days' => [
            'Monday' => 'Senin',
            'Sunday' => 'Ahad',
            'Tuesday' => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday' => 'Kamis',
            'Friday' => 'Jumat',
            'Saturday' => 'Sabtu'
        ],
        'months' => [
            'January' => 'Januari',
            'February' => 'Februari',
            'March' => 'Maret',
            'April' => 'April',
            'May' => 'Mei',
            'June' => 'Juni',
            'July' => 'Juli',
            'August' => 'Agustus',
            'September' => 'September',
            'October' => 'Oktober',
            'November' => 'Nopember',
            'December' => 'Desember'
        ]
    ];

    private static function get($element, $key) {
        return array_key_exists($key, static::$maps[$element]) ? static::$maps[$element][$key] : null;
    }

    public static function today()
    {
        static::$params = Carbon::today();

        return new static;
    }

    public static function convert($params)
    {
        static::$params = $params;

        return new static;
    }

    public static function addDays($sub)
    {
        static::$params = Carbon::parse(static::$params)->addDays($sub);

        return new static;
    }

    public static function addMonths($sub)
    {
        static::$params = Carbon::parse(static::$params)->addMonths($sub);

        return new static;
    }

    public static function ISO8601ToAmerican()
    {
        return empty(static::$params) ? null : Carbon::createFromFormat('Y-m-d', static::$params)->format('d/m/Y');
    }

    public static function americanToISO8601()
    {
        return empty(static::$params) ? null : Carbon::createFromFormat('d/m/Y', static::$params)->format('Y-m-d');
    }

    public static function ISO8601()
    {
        return empty(static::$params) ? null : Carbon::parse(static::$params)->format('Y-m-d');
    }

    public static function readableEnglish()
    {
        return empty(static::$params) ? null : Carbon::parse(static::$params)->format('j M Y');
    }

    public static function hijriDate()
    {
        $hijri = new HijriDate(strtotime(static::$params));

        return $hijri->get_date();
    }

    public static function readableNoDay()
    {
        $date = Carbon::parse(static::$params)->format('j');
        $month = Carbon::parse(static::$params)->format('F');
        $year = Carbon::parse(static::$params)->format('Y');

        $month = static::get('months', $month);

        return empty(static::$params) ? null : "{$date} {$month} {$year}";
    }

    public static function readable()
    {
        $day = Carbon::parse(static::$params)->format('l');
        $date = Carbon::parse(static::$params)->format('j');
        $month = Carbon::parse(static::$params)->format('F');
        $year = Carbon::parse(static::$params)->format('Y');

        $day = static::get('days', $day);
        $month = static::get('months', $month);

        return empty(static::$params) ? null : "{$day}, {$date} {$month} {$year}";
    }

    public static function diffInDays($start, $end)
    {
        $s = new Carbon($start);
        $e = new Carbon($end);

        return $s->diffInDays($e);
    }

    public static function diffInMonths($start, $end)
    {
        $s = new Carbon($start);
        $e = new Carbon($end);

        return $s->diffInMonths($e);
    }
}

/**
* Get hijri date from gregorian
*
* @author   Faiz Shukri
* @date     5 Dec 2013
* @url      https://gist.github.com/faizshukri/7802735
*
* Copyright 2013 | Faiz Shukri
* Released under the MIT license
*/
class HijriDate{

    private $hijri;

    public function __construct( $time = false ){
        if(!$time) $time = time();
        $this->hijri = $this->GregorianToHijri($time);
    }

    public function get_date(){
        return $this->hijri[1] . ' ' . $this->get_month_name($this->hijri[0]) . ' ' . $this->hijri[2] . ' H';
    }

    public function get_day(){
        return $this->hijri[1];
    }

    public function get_month(){
        return $this->hijri[0];
    }

    public function get_year(){
        return $this->hijri[2];
    }
    
    public function get_month_name($i){
        static $month  = array(
            "Muharram", "Safar", "Rabiulawal", "Rabiulakhir",
            "Jamadilawal", "Jamadilakhir", "Rejab", "Syaaban",
            "Ramadhan", "Syawal", "Zulkaedah", "Zulhijjah"
        );
        return $month[$i-1];
    }

    private function GregorianToHijri($time = null){
        if ($time === null) $time = time();
        $m = date('m', $time);
        $d = date('d', $time);
        $y = date('Y', $time);

        return $this->JDToHijri(cal_to_jd(CAL_GREGORIAN, $m, $d, $y));
    }

    private function HijriToGregorian($m, $d, $y){
        return jd_to_cal(CAL_GREGORIAN, $this->HijriToJD($m, $d, $y));
    }

    # Julian Day Count To Hijri
    private function JDToHijri($jd){
        $jd = $jd - 1948440 + 10632;
        $n  = (int)(($jd - 1) / 10631);
        $jd = $jd - 10631 * $n + 354;
        $j  = ((int)((10985 - $jd) / 5316)) *
            ((int)(50 * $jd / 17719)) +
            ((int)($jd / 5670)) *
            ((int)(43 * $jd / 15238));
        $jd = $jd - ((int)((30 - $j) / 15)) *
            ((int)((17719 * $j) / 50)) -
            ((int)($j / 16)) *
            ((int)((15238 * $j) / 43)) + 29;
        $m  = (int)(24 * $jd / 709);
        $d  = $jd - (int)(709 * $m / 24);
        $y  = 30*$n + $j - 30;

        return array($m, $d, $y);
    }

    # Hijri To Julian Day Count
    private function HijriToJD($m, $d, $y){
        return (int)((11 * $y + 3) / 30) +
            354 * $y + 30 * $m -
            (int)(($m - 1) / 2) + $d + 1948440 - 385;
    }
}