<?php
namespace App\Library;

class Phone
{
	
	public static function format0($phone)
	{
		// kadang ada penulisan no hp 0811 239 345
        $phone = str_replace(" ","",$phone);
        // kadang ada penulisan no hp (0274) 778787
        $phone = str_replace("(","",$phone);
        // kadang ada penulisan no hp (0274) 778787
        $phone = str_replace(")","",$phone);
        // kadang ada penulisan no hp 0811.239.345
        $phone = str_replace(".","",$phone);
    
        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($phone))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($phone), 0, 2)=='62'){
                $new_phone = '0'.substr(trim($phone), 2);
            }
            else if(substr(trim($phone), 0, 3)=='+62'){
                $new_phone = '0'.substr(trim($phone), 3);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($phone), 0, 1)=='0'){
                $new_phone = trim($phone);
            }
            else{
                $new_phone = $phone;
            }
        }
        else{
            $new_phone = $phone;
        }
        return $new_phone;
    }

    public static function format62($phone)
	{
		// kadang ada penulisan no hp 0811 239 345
        $phone = str_replace(" ","",$phone);
        // kadang ada penulisan no hp (0274) 778787
        $phone = str_replace("(","",$phone);
        // kadang ada penulisan no hp (0274) 778787
        $phone = str_replace(")","",$phone);
        // kadang ada penulisan no hp 0811.239.345
        $phone = str_replace(".","",$phone);
    
        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($phone))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($phone), 0, 3)=='+62'){
                $new_phone = '62'.substr(trim($phone), 3);
            }
            // cek apakah no hp karakter 1-2 adalah 62
            if(substr(trim($phone), 0, 2)=='62'){
                $new_phone = trim($phone);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($phone), 0, 1)=='0'){
                $new_phone = '62'.substr(trim($phone), 1);
            }
            else{
                $new_phone = $phone;
            }
        }
        else{
            $new_phone = $phone;
        }
        return $new_phone;
	}
	
}