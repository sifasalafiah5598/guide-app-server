<?php

namespace App\Library;

use Image;
use Illuminate\Support\Facades\Storage;

class Images
{
    private static $image, $path;

    public static function make($image, $width, $height, $original_img_name = null)
    {
        if (empty($image)) {
            return;
        }

        $name = $image->getClientOriginalName();

        $img_name = pathinfo($name, PATHINFO_FILENAME);

        $extension = pathinfo($name, PATHINFO_EXTENSION);

        static::$path = file_name(empty($original_img_name) ? $img_name : $original_img_name, $extension);
        
        static::$image = Image::make($image->getRealPath());
        
        // if (static::$image->width() > $width) { 
        //     static::$image->resize($width, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });
        // }
        
        // if (static::$image->height() > $height) {
        //     static::$image->resize(null, $height, function ($constraint) {
        //         $constraint->aspectRatio();
        //     }); 
        // }

        // static::$image->resizeCanvas($width, $height, 'center', false, '#ffffff');

        static::$image->encode();

        return new static;
    }

    public static function save($directory)
    {
        $path = $directory . '/' . static::$path;

        Storage::disk('public')->put($path, static::$image);

        return Storage::disk('public')->url($path);
    }
}
