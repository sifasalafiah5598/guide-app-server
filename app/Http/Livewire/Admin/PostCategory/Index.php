<?php

namespace App\Http\Livewire\Admin\PostCategory;

use Livewire\Component;
use Livewire\WithPagination;
use View;
use File;
use Response;

class Index extends Component
{
    use WithPagination;
    public $search = '';
    public $name = '';
    public $name_copy = '';
    public $checkbox = [
        0 => true,
        1 => true,
        2 => false,
        3 => false,
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function delete($id){
        $post_category = postCategory()->findOrFail($id);
        $post_category->ads()->delete();
        $post_category->posts()->delete();
        $post_category->delete();
        alert()->success('Berhasil','Kategori artikel berhasil dihapus');
        return redirect(route('admin.post-category.index'));
    }

    public function onBtnCancelClick(){
        $this->name = '';
    }

    public function onCancelCopy(){
        $this->name_copy = '';
    }

    public function createApp(){
        $validatedData = $this->validate([
            'name' => 'required',
        ]);
        $postCategory = auth()->user()->postCategory()->create($validatedData);
        alert()->success('Berhasil','Aplikasi berhasil ditambahkan');
        return redirect(route('admin.post.index', $postCategory->id));
    }

    public function copyApp($id){
        $validatedData = $this->validate([
            'name_copy' => 'required',
        ]);

        $apporigin = postCategory()->findOrFail($id);
        $postOrigin = $apporigin->posts;
        $adsOrigin = $apporigin->ads;
        $content = [];
        foreach ($postOrigin as $key => $value) {
            $content[] = [
                'title' => $value['title'],
                'post_content' => $value['post_content'],
            ];
        }
        $appDestination = $postCategory = auth()->user()->postCategory()->create([
            'name' => $this->name_copy
        ]);

        if($this->checkbox[0]===true || $this->checkbox[0]==='true'){
            $appDestination->privacy_policy = $apporigin->privacy_policy;
        }
        if($this->checkbox[2]===true || $this->checkbox[2]==='true'){
            $appDestination->publisher_id = $apporigin->publisher_id;
        }
        $appDestination->save();
        if($this->checkbox[1]===true || $this->checkbox[1]==='true'){
            $appDestination->posts()->createMany($content);
        }
        if($this->checkbox[3]===true || $this->checkbox[3]==='true'){
            $appDestination->ads()->create([
                'app_id' => $adsOrigin->app_id,
                'open' => $adsOrigin->open,
                'interstitial' => $adsOrigin->interstitial,
                'banner' => $adsOrigin->banner,
                'native' => $adsOrigin->native
            ]);
        }
        toast('Berhasil disalin!','success');
        return redirect(route('admin.post.index', $postCategory->id));
    }

    public function download($id){
        $userId = auth()->user()->id;
        $postCategory = auth()->user()->postCategory()->findOrFail($id);
        $content = [];
        foreach($postCategory->posts()->where('flag_active', true)->get() as $key=> $data){
            $content[] = [
                'id' => $key+1,
                'title' => $data->title,
                'post_content' => $data->post_content,
            ];
        }
        

        $JSON_FILE = json_encode([
            'app_name' => $postCategory->name,
            'privacy_policy' => $postCategory->privacy_policy,
            'admob' => [
                'publisher_id' => $postCategory->publisher_id,
                'app_id' => $postCategory->ads?$postCategory->ads->app_id:null,
                'banner' => $postCategory->ads?$postCategory->ads->banner:null,
                'interstitial' => $postCategory->ads?$postCategory->ads->interstitial:null,
                'native' => $postCategory->ads?$postCategory->ads->native:null,
                'open' => $postCategory->ads?$postCategory->ads->open:null,
            ],
            'content'=>$content
        ]);
        $file = 'data.json';
        $destinationPath = public_path()."/upload/json/$userId/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$JSON_FILE);
        return response()->download($destinationPath.$file);
    }

    public function render()
    {
        return view('livewire.admin.post-category.index',[
            'post_categories' => auth()->user()->postCategory()->where(function($query) {
                $query->where('name', 'like', '%'.$this->search.'%');
            })->paginate(11),
        ])->layout('layouts.admin.home');
    }
}
