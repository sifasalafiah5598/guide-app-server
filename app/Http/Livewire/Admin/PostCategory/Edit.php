<?php

namespace App\Http\Livewire\Admin\PostCategory;

use Livewire\Component;

class Edit extends Component
{
    public $post_category, $name, $code;

    public function mount($id){
        $post_category = postCategory()->findOrFail($id);
        $this->post_category = $post_category;
        $this->name = $post_category->name;
        $this->code = $post_category->code;
    }

    protected $messages = [
        'required' => 'Kolom isian tidak boleh kosong',
    ];

    public function store(){
        $validatedData = $this->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        
        $this->post_category->update($validatedData);
        alert()->success('Berhasil','Kategori artikel berhasil diubah');
        return redirect(route('admin.post-category.index'));
    }

    public function render()
    {
        return view('livewire.admin.post-category.edit')->layout('layouts.admin.index');
    }
}
