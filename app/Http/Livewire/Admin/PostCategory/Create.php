<?php

namespace App\Http\Livewire\Admin\PostCategory;

use Livewire\Component;

class Create extends Component
{
    public $name;
    public $app_id;
    public $banner;
    public $open;
    public $native;
    public $interstitial;

    protected $messages = [
        'required' => 'Kolom isian tidak boleh kosong',
    ];


    public function store(){
        $validatedData = $this->validate([
            'name' => 'required|unique:post_categories,name,null,null,user_id,'.auth()->user()->id,
        ]);
        $validatedAdsData = $this->validate([
            'app_id' => 'required',
            'open' =>'required',
            'native' =>'required',
            'interstitial' =>'required',
            'banner' =>'required',
        ]);
        
        $postCategory = auth()->user()->postCategory()->create($validatedData);
        $postCategory->ads()->create($validatedAdsData);
        alert()->success('Berhasil','Aplikasi berhasil ditambahkan');
        return redirect(route('admin.post.index', $postCategory->id));
    }

    public function render()
    {
        return view('livewire.admin.post-category.create')->layout('layouts.admin.index');
    }
}
