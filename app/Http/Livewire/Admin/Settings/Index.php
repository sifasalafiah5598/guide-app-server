<?php

namespace App\Http\Livewire\Admin\Settings;

use Livewire\Component;

class Index extends Component
{
    public $post_category_id, $name, $publisher_id, $privacy_policy, $package_name;

    public function mount($id){
        $this->post_category_id = $id;
        $data = auth()->user()->postCategory()->findOrFail($this->post_category_id);
        $this->name = $data->name;
        $this->publisher_id = $data->publisher_id;
        $this->privacy_policy = $data->privacy_policy;
        $this->package_name = $data->package_name;
    }

    public function saveData(){
        $validatedAdsData = $this->validate([
            'name' => 'required',
            'publisher_id' =>'nullable',
            'privacy_policy' =>'nullable',
            'package_name' =>'nullable',
        ]);

        $data = auth()->user()->postCategory()->findOrFail($this->post_category_id);
        $data->update($validatedAdsData);
        toast('Berhasil disimpan!','success');
        return redirect(route('admin.setting.index', $this->post_category_id));
    }

    public function render()
    {
        return view('livewire.admin.settings.index')->layout('layouts.admin.index',['post_category_id' => $this->post_category_id]);
    }
}
