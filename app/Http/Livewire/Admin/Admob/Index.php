<?php

namespace App\Http\Livewire\Admin\Admob;

use Livewire\Component;

class Index extends Component
{
    public $post_category_id;
    public $app_id, $native, $open, $interstitial, $banner;

    public function mount($id){
        $this->post_category_id = $id;
        $data = auth()->user()->postCategory()->findOrFail($this->post_category_id);
        if($data->ads!=null){
            $this->app_id = $data->ads->app_id;
            $this->native = $data->ads->native;
            $this->open = $data->ads->open;
            $this->interstitial = $data->ads->interstitial;
            $this->banner = $data->ads->banner;            
        }
    }

    public function saveData(){
        $validatedAdsData = $this->validate([
            'app_id' => 'required',
            'open' =>'nullable',
            'native' =>'required',
            'interstitial' =>'required',
            'banner' =>'required',
        ]);
        $data = auth()->user()->postCategory()->findOrFail($this->post_category_id);
        $data->ads()->delete();
        $data->ads()->create($validatedAdsData);
        toast('Berhasil disimpan!','success');
        return redirect(route('admin.admob.index', $this->post_category_id));
    }

    public function render()
    {
        return view('livewire.admin.admob.index')->layout('layouts.admin.index',['post_category_id' => $this->post_category_id]);
    }
}
