<?php

namespace App\Http\Livewire\Admin\Post;

use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use WithFileUploads;
    public $post, $title, $post_content, $post_category_id, $photo;
    public $post_categories = [];
    
    public function mount($id){
        $post = post()->findOrFail($id);
        $this->post = $post;
        $this->title = $post->title;
        $this->post_content = $post->post_content;
        $this->post_category_id = $post->post_category_id;
        $this->post_categories = postCategory()->get();
    }

    protected $messages = [
        'required' => 'Kolom isian tidak boleh kosong',
    ];
    
    public function store(){
        $validatedData = $this->validate([
            'title' => 'required',
            'post_content' => 'required',
            'post_category_id' => 'required',
        ]);
        $this->post->update($validatedData);
        if(!empty($this->photo)){
            $photo = $this->photo->store('public/photos');
            $this->post->photo = $photo;
            $this->post->save();
        }
        toast('Konten berhasil diubah!','success');
        return redirect(route('admin.post.index', $this->post->post_category_id));
    }

    public function render()
    {
        return view('livewire.admin.post.edit')->layout('layouts.admin.index',['post_category_id' => $this->post->post_category_id]);
    }
}
