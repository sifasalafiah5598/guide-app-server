<?php

namespace App\Http\Livewire\Admin\Post;

use Livewire\Component;
use Livewire\WithPagination;
use View;
use File;
use Response;

class Index extends Component
{
    use WithPagination;
    public $search = '';
    public $post_category_id;

    public function mount($id){
        $this->post_category_id = $id;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function delete($id){
        $post = post()->findOrFail($id);
        $post->delete();
        toast('Konten berhasil dihapus!','success');
        return redirect(route('admin.post.index', $this->post_category_id));
    }

    public function downloadJSON(){
        $userId = auth()->user()->id;
        $postCategory = auth()->user()->postCategory()->findOrFail($this->post_category_id);
        $content = [];
        foreach($postCategory->posts()->where('flag_active', true)->get() as $key=> $data){
            $content[] = [
                'id' => $key+1,
                'title' => $data->title,
                'post_content' => $data->post_content,
            ];
        }
        

        $JSON_FILE = json_encode([
            'app_name' => $postCategory->name,
            'privacy_policy' => $postCategory->privacy_policy,
            'admob' => [
                'publisher_id' => $postCategory->publisher_id,
                'app_id' => $postCategory->ads?$postCategory->ads->app_id:null,
                'banner' => $postCategory->ads?$postCategory->ads->banner:null,
                'interstitial' => $postCategory->ads?$postCategory->ads->interstitial:null,
                'native' => $postCategory->ads?$postCategory->ads->native:null,
                'open' => $postCategory->ads?$postCategory->ads->open:null,
            ],
            'content'=>$content
        ]);
        $file = 'data.json';
        $destinationPath = public_path()."/upload/json/$userId/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$JSON_FILE);
        return response()->download($destinationPath.$file);
    }

    public function switch($id, $status){
        $post = post()->findOrFail($id);
        $post->flag_active = $status;
        $post->save();
    }
    public function render()
    {
        return view('livewire.admin.post.index',[
            'posts' => auth()->user()->postCategory()->findOrFail($this->post_category_id)->posts()->where(function($query) {
                $query->where('title', 'like', '%'.$this->search.'%');
            })->paginate(50),
        ])->layout('layouts.admin.index',['post_category_id' => $this->post_category_id]);
    }
}
