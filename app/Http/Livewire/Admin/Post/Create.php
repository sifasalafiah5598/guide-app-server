<?php

namespace App\Http\Livewire\Admin\Post;

use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use WithFileUploads;
    public $title, $post_content, $post_category_id, $photo;

    
    public function mount($id){
        $this->post_category_id = $id;
    }

    protected $messages = [
        'required' => 'Kolom isian tidak boleh kosong',
    ];

    public function store(){

        $validatedData = $this->validate([
            'title' => 'required',
            'post_content' => 'required',
            'post_category_id' => 'required',
        ]);

        $post = post()->create($validatedData);
        if(!empty($this->photo)){
            $photo = $this->photo->store('public/photos');
            $post->photo = $photo;
            $post->save();
        }
        toast('Konten berhasil ditambahkan!','success');
        return redirect(route('admin.post.index', $this->post_category_id));
    }
    
    public function render()
    {
        return view('livewire.admin.post.create')->layout('layouts.admin.index',['post_category_id' => $this->post_category_id]);
    }
}
