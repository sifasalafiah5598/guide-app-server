<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function app($id)
    {
        $postCategory = postCategory()->findOrFail($id);

        $content = [];
        foreach($postCategory->posts()->where('flag_active', true)->get() as $key => $data){
            $content[] = [
                'id' => $key++,
                'title' => $data->title,
                'post_content' => $data->post_content,
                'photo' => config('app.url').'/storage/'.substr($data->photo,7)
            ];
        }

        $metric = $postCategory->apis()->now()->firstOrCreate([
            'date' => now()->format('Y-m-d'),
            'hour' => now()->format('H')
        ]);
        $metric->increment('calls');
        
        return response()->json(
            [
                'app_name' => $postCategory->name,
                'privacy_policy' => $postCategory->privacy_policy,
                'cover_image' => config('app.url').'/storage/'.substr($postCategory->posts()->where('flag_active', true)->first()->photo,7),
                'admob' => [
                    'publisher_id' => Str::of($postCategory->publisher_id)->trim(),
                    'app_id' => $postCategory->ads ? Str::of($postCategory->ads->app_id)->trim() : null,
                    'banner' => $postCategory->ads ? Str::of($postCategory->ads->banner)->trim() : null,
                    'interstitial' => $postCategory->ads ? Str::of($postCategory->ads->interstitial)->trim() : null,
                    'native' => $postCategory->ads ? Str::of($postCategory->ads->native)->trim() : null,
                    'open' => $postCategory->ads ? Str::of($postCategory->ads->open)->trim() : null,
                ],
                'content'=>$content
            ]
        );
    }
}
