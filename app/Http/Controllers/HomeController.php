<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $apps = auth()->user()->postCategory()->where(function($query) use ($request){
            $query->where('name', 'like', '%'.$request->search.'%');
        })->paginate(11);
        return view('home', ['apps' => $apps]);
    }
}
