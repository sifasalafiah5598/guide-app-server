$(document).ready(function () {
    var currentDate = new Date();
    $('#datepicker').datepicker({
    format: 'yyyy/mm/dd',
    autoclose:true,
    endDate: "currentDate",
    language: 'id',
    maxDate: currentDate
    }).on('changeDate', function (ev) {
       $(this).datepicker('hide');
    });
    $('.datepicker').keyup(function () {
       if (this.value.match(/[^0-9]/g)) {
          this.value = this.value.replace(/[^0-9^-]/g, '');
       }
    });
 });