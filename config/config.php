<?php

return [
    'company' => [
        'name' => 'company_name',
        'address' => 'company_address',
        'email' => 'company_email',
        'phone' => 'company_phone',
        'stakeholder' => 'company_stakeholder'
    ]
];