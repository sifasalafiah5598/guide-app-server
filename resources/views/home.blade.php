<x-coda-base title="Home">
    <div class="layout--app">
        <main class="content">
            <div class="content__inner">
                <div class="ui container-fluid content__body p-3">
                    <table class="ui unstackable table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Aplikasi</th>
                                <th>Package Name</th>
                                <th>Konten | Pengaturan | Ads</th>
                                <th>Api Endpoint</th>
                                <th>Api Calls Today</th>
                                <th>Jumlah Konten</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($apps as $app)
                            <tr>
                                <td>{{ (($apps->currentPage() - 1) * $apps->perPage()) + $loop->iteration }}</td>
                                <td>{{ $app->name }}</td>
                                <td>{{ $app->package_name }}</td>
                                <td>
                                <x-coda-label color="{{$app->posts->count()==0?'yellow':'green'}} solid"><i class="{{$app->posts->count()==0?'exclamation circle':'check'}} icon"></i></x-coda-label>
                                <x-coda-label color="{{$app->privacy_policy=='' || $app->publisher_id==''?'yellow':'green'}} solid"><i class="{{$app->posts->count()==0?'exclamation circle':'check'}} icon"></i></x-coda-label>
                                <x-coda-label color="{{$app->ads == null?'yellow':'green'}} solid"><i class="{{$app->ads == null?'exclamation circle':'check'}} icon"></i></x-coda-label>
                                </td>
                                <td>{{ config('app.url').'/api/app/'.$app->id }}</td>
                                <td>{{ $app->apis()->today()->sum('calls') }}</td>
                                <td>{{ $app->posts->count() }}</td>
                                <td>
                                    <a href="" class="mini circular ui icon button teal">
                                        <i class="icon eye"></i>
                                    </a>
                                    <a href="" class=" mini circular ui icon button red">
                                        <i class="icon trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </main>
    </div>
</x-coda-base>