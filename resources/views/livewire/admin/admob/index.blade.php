<div>
    <form wire:submit.prevent="saveData" id="form">
        <div class="alert alert-warning" role="alert">Mode ONLINE tidak mendukung App Open Ads, admob app open id diisi ketika akan menggunakan mode OFFLINE.</div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Monetisasi Admob</h6>
            </div>
            <div class="card-body">
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>App Id</label><span style="color:red"> *</span>
                        <input wire:model.prevent="app_id" type="text" class="adunit form-control @error('app_id') is-invalid @enderror"
                            placeholder="Masukkan App Id Admob">
                        @error('app_id')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Banner</label><span style="color:red"> *</span>
                        <input wire:model.prevent="banner" type="text" class="adunit form-control @error('banner') is-invalid @enderror"
                            placeholder="Masukkan Banner Ads Id">
                        @error('banner')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Interstitial</label><span style="color:red"> *</span>
                        <input wire:model.prevent="interstitial" type="text" class="adunit form-control @error('interstitial') is-invalid @enderror"
                            placeholder="Masukkan Interstitial Ads Id">
                        @error('interstitial')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Native</label><span style="color:red"> *</span>
                        <input wire:model.prevent="native" type="text" class="adunit form-control @error('native') is-invalid @enderror"
                            placeholder="Masukkan Native Ads Id">
                        @error('native')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Open</label>
                        <input wire:model.prevent="open" type="text" class="adunit form-control @error('open') is-invalid @enderror"
                            placeholder="Masukkan Open Ads Id">
                        @error('open')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" wire:loading.attr="disabled" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Simpan</button>
                </div>
            </div>
        </div>
    </form>
    @push('scripts')
    <script>
        $(function() {
            $('.adunit').on('keypress', function(e) {
                if (e.which == 32){
                    console.log('Space Detected');
                    return false;
                }
            });
        });
    </script>
    <script>
        document.addEventListener('livewire:load', function () {
            document.getElementById("nav_admob").classList.add('active');
        })
    </script>
    @endpush
</div>
