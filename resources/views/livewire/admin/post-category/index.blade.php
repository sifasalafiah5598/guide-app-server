<div>
    <div class="form-row col-12 mb-3">
        <div class="form-group d-flex col-lg-9 col-md-9 col-sm-12 p-0 justify-content-start">
            <div class="input-group">
                <input wire:model="search" type="text" class="form-control bg-white" placeholder="Cari di sini"
                    aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-search fa-sm"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group d-flex col-lg-3 col-md-3 col-sm-12 p-0 justify-content-end">
            <button class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#addApp">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Tambah Aplikasi Baru</span>
            </button>
        </div>
    </div>
    <!-- Modal Create-->
    <div class="modal fade" id="addApp" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="addAppLabel" aria-hidden="true" wire:ignore.self>
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addAppLabel">Tambah Aplikasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="createApp" id="form">
                    <div class="modal-body">
                        <div class="form-row mb-3">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                            <label>Nama Aplikasi</label><span style="color:red"> *</span>
                            <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                placeholder="Masukkan nama aplikasi">
                            @error('name')
                            <span class="invalid-feedback">
                                {{ $message }}
                            </span>
                            @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click="onBtnCancelClick()">Batal</button>
                        <button type="submit" class="btn btn-primary {{$name==''?'disabled':''}}" >Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Aplikasi</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Aplikasi</th>
                            <th>Package Name</th>
                            <th>Konten | Pengaturan | Ads</th>
                            <th>Api Endpoint</th>
                            <th>Api Calls Today</th>
                            <th>Jumlah Konten</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($post_categories as $key=> $post_category)
                        <tr>
                            <td>{{ (($post_categories->currentPage() - 1) * $post_categories->perPage()) + $loop->iteration }}
                            </td>
                            <td><a href="{{route('admin.post.index', $post_category->id)}}">{{ $post_category->name }}</a></td>
                            <td>{{ $post_category->package_name }}</td>
                            <td>
                                <button class="btn {{$post_category->posts->count()==0?'btn-warning':'btn-success'}} btn-circle btn-sm">
                                    <i class="fas {{$post_category->posts->count()==0?'fa-exclamation-triangle':'fa-check'}}"></i>
                                </button>
                                <button class="btn {{$post_category->privacy_policy=='' || $post_category->publisher_id==''?'btn-warning':'btn-success'}} btn-circle btn-sm">
                                    <i class="fas {{$post_category->privacy_policy=='' || $post_category->publisher_id==''?'fa-exclamation-triangle':'fa-check'}}"></i>
                                </button>
                                <button class="btn btn-success {{$post_category->ads == null ?'btn-warning':'btn-success'}} btn-circle btn-sm">
                                    <i class="fas {{$post_category->ads == null ?'fa-exclamation-triangle':'fa-check'}}"></i>
                                </button>
                            </td>
                            <td>{{ config('app.url').'/api/app/'.$post_category->id }} 
                                <button class="btn btn-default btn-sm" onclick="copyText('{{config('app.url').'/api/app/'.$post_category->id}}')">
                                    <i class="fas fa-copy"></i>
                                </button></td>
                            <td>{{ $post_category->apis()->today()->sum('calls') }}</td>
                            <td>{{ $post_category->posts->count() }}</td>
                            <td>
                                <button class="btn btn-primary btn-circle btn-sm" data-toggle="modal" data-target="#copyModal{{$post_category->id}}">
                                    <i class="fas fa-copy"></i>
                                </button>
                                <button class="btn btn-primary btn-circle btn-sm" wire:click="download({{$post_category->id}})">
                                    <i class="fas fa-download"></i>
                                </button>
                                <a href="{{route('admin.post.index', $post_category->id)}}"
                                    class="btn btn-info btn-circle btn-sm">
                                    <i class="fas fa-eye"></i>
                                </a>
                                <button class="btn btn-danger btn-circle btn-sm" data-toggle="modal" data-target="#deleteModal{{$post_category->id}}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <!-- Modal Delete-->
                        <div class="modal fade" id="deleteModal{{$post_category->id}}" tabindex="-1"
                            aria-labelledby="deleteModal{{$post_category->id}}Label" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteModal{{$post_category->id}}Label">Hapus</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Apakah Anda yakin ingin menghapus {{$post_category->name}}?
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-secondary btn-icon-split mr-2" data-dismiss="modal">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-window-close"></i>
                                            </span>
                                            <span class="text">Batalkan</span>
                                        </button>
                                        <button wire:click="delete({{$post_category->id}})"
                                            class="btn btn-danger btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="text">Hapus</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modal Delete-->
                        <!-- Modal Copy-->
                        <div wire:ignore.self class="modal fade" id="copyModal{{$post_category->id}}" tabindex="-1"
                            aria-labelledby="copyModal{{$post_category->id}}Label" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="copyModal{{$post_category->id}}Label">Salin</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form wire:submit.prevent="copyApp({{$post_category->id}})" id="form">
                                        <div class="modal-body">
                                            <div class="form-row mb-3">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                <label>Nama Aplikasi</label><span style="color:red"> *</span>
                                                <input wire:model="name_copy" type="text" class="form-control @error('name_copy') is-invalid @enderror"
                                                    placeholder="Masukkan nama aplikasi">
                                                @error('name_copy')
                                                <span class="invalid-feedback">
                                                    {{ $message }}
                                                </span>
                                                @enderror
                                                </div>
                                            </div>
                                            <div>
                                                <input wire:model="checkbox.0" type="checkbox" id="checkPrivacyPolicy" name="checkPrivacyPolicy" value="true">
                                                <label for="checkPrivacyPolicy"> Salin privacy policy link</label><br>
                                                <input wire:model="checkbox.1" type="checkbox" id="checkContent" name="checkContent" value="true">
                                                <label for="checkContent"> Salin konten</label><br>
                                                <input wire:model="checkbox.2" type="checkbox" id="checkAdmobPubId" name="checkAdmobPubId" value="true">
                                                <label for="checkAdmobPubId"> Salin admob publisher id</label><br>
                                                <input wire:model="checkbox.3" type="checkbox" id="checkAdmobAdUnitId" name="checkAdmobAdUnitId" value="true">
                                                <label for="checkAdmobAdUnitId"> Salin admob ad unit id</label><br>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click="onCancelCopy()">Batal</button>
                                            <button type="submit" class="btn btn-primary {{$name_copy==''?'disabled':''}}" >Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End Modal Copy-->
                        @empty
                        <td colspan="10">
                            <h6 class="text-center" style="color:red">Tidak ada data yang tersedia</h6>
                        </td>
                        @endforelse
                    </tbody>
                </table>
                {{ $post_categories->links() }}
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        document.addEventListener('livewire:load', function () {
            document.getElementById("nav_post_category").classList.add('active');
        })
    </script>
    <script>
        function copyText(text) {
        var copyText = text;
        navigator.clipboard.writeText(copyText);
        }
    </script>
    @endpush
</div>
