<div>
    <form wire:submit.prevent="store" id="form">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Ubah Keyword Content</h6>
            </div>
            <div class="card-body">
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Nama Keyword Content</label><span style="color:red"> *</span>
                        <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            placeholder="Masukkan nama keyword content">
                        @error('name')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Kode Keyword Content</label><span style="color:red"> *</span>
                        <input wire:model="code" type="text" class="form-control @error('code') is-invalid @enderror"
                            placeholder="Masukkan kode keyword content">
                        @error('code')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <a href="{{route('admin.post-category.index')}}" class="btn btn-secondary btn-icon-split mr-2">
                        <span class="icon text-white-50">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Kembali</a>
                    <button type="submit" wire:loading.attr="disabled" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Simpan</button>
                </div>
            </div>
        </div>
    </form>
    @push('scripts')
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script>
        document.addEventListener('livewire:load', function () {
            document.getElementById("nav_post_category").classList.add('active');
        })
        var meta_description = document.getElementById("meta_description");
        CKEDITOR.replace(meta_description, {
            language: 'en-gb'
        });
        CKEDITOR.instances['meta_description'].on('change', function(e){
        @this.set('meta_description', e.editor.getData());
        });
        CKEDITOR.config.allowedContent = true;
    </script>
    @endpush
</div>
