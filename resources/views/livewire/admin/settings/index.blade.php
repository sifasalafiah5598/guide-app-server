<div>
    <form wire:submit.prevent="saveData" id="form">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Pengaturan Umum</h6>
            </div>
            <div class="card-body">
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Nama Aplikasi</label><span style="color:red"> *</span>
                        <input wire:model.prevent="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            placeholder="Masukkan nama aplikasi">
                        @error('name')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Package name</label>
                        <input wire:model.prevent="package_name" type="text" class="adunit form-control @error('package_name') is-invalid @enderror"
                            placeholder="Masukkan package name aplikasi">
                        @error('package_name')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Admob Publisher Id</label>
                        <input wire:model.prevent="publisher_id" type="text" class="adunit form-control @error('publisher_id') is-invalid @enderror"
                            placeholder="Masukkan publisher id">
                        @error('publisher_id')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-3">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label>Link PrivacyPolicy</label>
                        <input wire:model.prevent="privacy_policy" type="text" class="adunit form-control @error('privacy_policy') is-invalid @enderror"
                            placeholder="Masukkan link privacy policy">
                        @error('privacy_policy')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" wire:loading.attr="disabled" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Simpan</button>
                </div>
            </div>
        </div>
    </form>
    @push('scripts')
    <script>
        $(function() {
            $('.adunit').on('keypress', function(e) {
                if (e.which == 32){
                    console.log('Space Detected');
                    return false;
                }
            });
        });
    </script>
    <script>
        document.addEventListener('livewire:load', function () {
            document.getElementById("nav_setting").classList.add('active');
        })
    </script>
    @endpush
</div>
