<div>
    <form wire:submit.prevent="store" id="form">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Ubah Artikel</h6>
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                    <label>Cover image</label>
                    <input type="file" wire:model="photo">
                        @error('photo') <span class="invalid-feedback">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                        <label>Judul</label><span style="color:red"> *</span>
                        <input wire:model.defer="title" type="text"
                            class="form-control @error('title') is-invalid @enderror"
                            placeholder="Masukkan judul artkel">
                        @error('title')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                    <div wire:ignore class="col-lg-12 col-md-12 col-sm-12 mb-3">
                        <label>Konten</label><span style="color:red"> *</span>
                        <textarea id="post_content" class="form-control @error('post_content') is-invalid @enderror"
                            name="post_content" rows="10" cols="50">{{$post_content}}</textarea>
                        @error('post_content')
                        <span class="invalid-feedback">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end">
            <button onclick="window.history.back()" class="btn btn-secondary btn-icon-split mr-2" data-dismiss="modal">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Kembali</span>
            </button>
            <button type="submit" wire:loading.attr="disabled" class="btn btn-success btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Simpan</span>
            </button>
        </div>
    </form>
    @push('scripts')
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script>
        $('#metaTagsSelect').select2({
            theme: 'bootstrap4',
        });
        $('#metaTagsSelect').on('change', function () {
            @this.meta_tag_id = $(this).val();
        });
        $('#categorySelect').select2({
            theme: 'bootstrap4',
        });
        $('#categorySelect').on('change', function () {
            @this.post_category_id = $(this).val();
        });
        document.addEventListener('livewire:load', function (event) {
            document.getElementById("nav_employee").classList.add('active');
            window.livewire.hook('afterDomUpdate', () => {});
        });
        document.addEventListener('livewire:load', function () {
            document.getElementById("nav_post").classList.add('active');
        })
        var post_content = document.getElementById("post_content");
        CKEDITOR.replace(post_content, {
            language: 'en-gb',
            allowedContent:
                'h1 h2 h3 p blockquote strong em ul li;' +
                'a[!href];' +
                'img(left,right)[!src,alt,width,height];'
        });
        CKEDITOR.instances['post_content'].on('change', function(e){
        @this.set('post_content', e.editor.getData());
        });
        CKEDITOR.config.allowedContent = true;
    </script>
    <script>
       document.addEventListener('livewire:load', function () {
           document.getElementById("nav_content").classList.add('active');
       })
    </script>
    @endpush
</div>
