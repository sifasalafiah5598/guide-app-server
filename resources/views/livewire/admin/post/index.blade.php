<div>
    <div class="form-row col-12 mb-3">
        <div class="form-group d-flex col-lg-3 col-md-3 col-sm-12 p-0">
            <button wire:click="downloadJSON()" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Download JSON</span>
            </button>
        </div>
        <div class="form-group d-flex col-lg-3 col-md-3 col-sm-12 p-0">
            <a href="{{route('admin.post.create', $post_category_id)}}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Tambah Konten</span>
            </a>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Konten Aplikasi {{auth()->user()->postCategory()->findOrFail($post_category_id)->name}}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($posts as $key=> $post)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $post->title }}</td>
                            <td>
                                <div class="custom-control custom-switch">
                                    @if($post->flag_active)
                                    <input checked type="checkbox" class="custom-control-input" id="switch{{$post->id}}" wire:click="switch({{$post->id}}, false)">
                                    @else
                                    <input type="checkbox" class="custom-control-input" id="switch{{$post->id}}" wire:click="switch({{$post->id}}, true)">
                                    @endif
                                    <label class="custom-control-label" for="switch{{$post->id}}"></label>
                                </div>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-circle btn-sm" data-toggle="modal" data-target="#deleteModal{{$post->id}}">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <a href="{{route('admin.post.edit', $post->id)}}" class="btn btn-warning btn-circle btn-sm">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        <!-- Modal Delete-->
                        <div class="modal fade" id="deleteModal{{$post->id}}" tabindex="-1" aria-labelledby="deleteModal{{$post->id}}Label" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteModal{{$post->id}}Label">Hapus Artikel</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Apakah Anda yakin ingin menghapus artikel {{$post->title}}?
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-secondary btn-icon-split mr-2" data-dismiss="modal">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-window-close"></i>
                                            </span>
                                            <span class="text">Batalkan</span>
                                        </button>
                                        <button wire:click="delete({{$post->id}})" class="btn btn-danger btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-trash"></i>
                                            </span>
                                            <span class="text">Hapus</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modal Delete-->
                        @empty
                        <td colspan="10">
                            <h6 class="text-center" style="color:red">Tidak ada data yang tersedia</h6>
                        </td>
                        @endforelse
                    </tbody>
                </table>
                {{ $posts->links() }}
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function () {
            document.getElementById("nav_content").classList.add('active');
        })
    </script>
</div>
