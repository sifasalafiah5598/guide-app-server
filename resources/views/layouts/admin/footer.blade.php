<!-- Footer -->
<footer class="sticky-footer bg-white no-printme">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Hak Cipta Terpelihara</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->