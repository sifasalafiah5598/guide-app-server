<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
    <div class="sidebar-brand-text mx-3">Panel</div>
  </a>

<!-- Divider -->
<hr class="sidebar-divider">
  
  <!-- Heading -->
  <div class="sidebar-heading">
    Konten
  </div>

<!-- Nav -->
<li id="nav_content" class="nav-item">
    <a class="nav-link" href="{{route('admin.post.index', $post_category_id)}}">
      <i class="fas fa-fw fa-bars"></i>
      <span>Konten</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Pengaturan
  </div>

<!-- Nav -->
<li id="nav_setting" class="nav-item">
    <a class="nav-link" href="{{route('admin.setting.index', $post_category_id)}}">
      <i class="fas fa-fw fa-cogs"></i>
      <span>Umum</span></a>
  </li>

  <!-- Nav -->
<li id="nav_admob" class="nav-item">
    <a class="nav-link" href="{{route('admin.admob.index', $post_category_id)}}">
      <i class="fas fa-fw fa-ad"></i>
      <span>Admob</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Beranda
  </div>

<!-- Nav -->
<li id="nav_" class="nav-item">
    <a class="nav-link" href="{{route('admin.post-category.index')}}">
      <i class="fas fa-fw fa-home"></i>
      <span>Beranda</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">
  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->