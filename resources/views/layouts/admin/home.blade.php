@extends('layouts.admin.template')

@section('css')
@endsection

@section('content')
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        @include('layouts.admin.topbar')
        <!-- Begin Page Content -->
        <div class="container-fluid">
          {{ $slot }}
        </div>
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
@endsection
@section('js')
@endsection