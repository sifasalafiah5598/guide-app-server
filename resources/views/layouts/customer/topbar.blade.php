<header class="bg-white">
    <div class="header-top-furniture wrapper-padding-2 res-header-sm">
        <div class="container-fluid">
            <div class="header-bottom-wrapper">
                <div class="logo-2 furniture-logo ptb-30">
                    <a href="/home">
                        <img src="{{asset('img/logo/trisnomotor2.png')}}" alt="">
                    </a>
                </div>
                @php $phone = _config()->companyPhone()->first()->value; $message = "Halo" @endphp
                <div class="menu-style-2 furniture-menu menu-hover">
                    <nav>
                        <ul>
                            <li><a href="/">Beranda</a></li>
                            <li><a href="{{route('product.index')}}">Beli Mobil</a></li>
                            <li><a href="{{"https://wa.me/$phone?text=$message"}}">Jual Mobil</a></li>
                            <li><a href="{{route('article.index')}}">Artikel</a></li>
                            <li><a href="#footer">Tentang Kami</a></li>
                            <li><a href="{{route('login')}}">{{auth()->check()?"Dashboard":"Login"}}</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="mobile-menu-area d-md-block col-md-12 col-lg-12 col-12 d-lg-none d-xl-none">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul>
                                <li><a href="/">Beranda</a></li>
                                <li><a href="{{route('product.index')}}">Beli Mobil</a></li>
                                <li><a  href="{{"https://wa.me/$phone?text=$message"}}">Jual Mobil</a></li>
                                <li><a href="{{route('article.index')}}">Artikel</a></li>
                                <li><a href="#footer">Tentang Kami</a></li>
                                <li><a href="{{route('login')}}">{{auth()->check()?"Dashboard":"Login"}}</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
