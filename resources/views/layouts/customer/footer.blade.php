<footer id="footer" class="footer-area">
    <div class="footer-top-area bg-img pt-105 pb-65" style="background-image: url(assets/img/bg/1.jpg)"
        data-overlay="9">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="footer-widget mb-40">
                        <h3 class="footer-widget-title">Tentang Kami</h3>
                        <div class="footer-newsletter">
                            <p>Trisno Motor adalah salah satu platform jual beli mobil bekas terpercaya sejak 2015.
                            </p>
                            <div class="footer-contact pt-3 ">
                                <p><span><i class="ti-location-pin"></i></span>{{_config()->companyAddress()->first()->value}}</p>
                                <p><span><i class=" ti-headphone-alt "></i></span>+{{_config()->companyPhone()->first()->value}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-3 d-flex align-item-center justify-content-center ">
                    <div class="footer-widget mb-40 text-center">
                        <h3 class="footer-widget-title ">Jual Beli</h3>
                        <div class="footer-widget-content">
                            <ul>
                                <li><a href="{{route('product.index')}}">Beli Mobil</a></li>
                                <li><a href="#">Jual Mobil</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-3 d-flex align-item-center justify-content-center text-center">
                    <div class="footer-widget mb-40">
                        <h3 class="footer-widget-title">Artikel & Review</h3>
                        <div class="footer-widget-content">
                            <ul>
                                <li><a href="{{route('article.index')}}">Artikel</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-bottom black-bg ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="copyright">
                        <p>
                            Copyright © 2021 <a href="https://trisnomotor.com/">Trisno Motor</a> || developed by 
                            <a href="https://codakarta.com/">Codakarta</a> || All Right Reserved.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
